const fse = require('fs-extra')
const _ = require('lodash')
const os = require('os')
const path = require('path')

const { Calendar, Event } = require('./index')


function tmpPath() {
  return path.join(
    os.tmpdir(),
    '' + Math.floor(Math.random() * 1000000),
  )
}


const testPath = tmpPath()
beforeEach(() => {
  fse.emptyDirSync(testPath)
})
afterAll(() => {
  fse.removeSync(testPath)
})


describe('Event', () => {
  let ev
  beforeEach(() => {
    ev = new Event({})
    ev.getSlug = async () => 'test'
    ev.getFrontmatter = async () => ({})
    ev.getContentHtml = async () => ''
    ev.getContentText = async () => ''
  })
  describe('getSlug()', () => {
    test('returns the name of the parent dir', () => {
      const ev = new Event({ thisPath: '/ab/cd/index.md' })
      expect(ev.getSlug()).toEqual('cd')
    })
  })
  describe('getOrganizer()', () => {
    test('from frontmatter', async () => {
      ev.getFrontmatter = async () => ({
        organizer: { name: 'Test', email: 'test@example.com' },
      })
      expect(await ev.getOrganizer()).toEqual({
        name: 'Test',
        email: 'test@example.com',
      })
    })
    test('vicpah default for vicpah group', async () => {
      ev.getFrontmatter = async () => ({
        eventGroups: ['vicpah']
      })
      expect(await ev.getOrganizer()).toEqual({
        name: 'VicPAH',
        email: 'bark@vicpah.org.au',
      })
    })
    test('null in other cases', async () => {
      expect(await ev.getOrganizer()).toBe(null)
    })
  })
  describe('getLocation()', () => {
    const defaultAdr = {
      name: 'testname',
      street_address: 'teststreet',
      suburb: 'testsuburb',
      state: 'teststate',
      country: 'testcountry',
    }
    test('all fields', async () => {
      ev.getFrontmatter = async () => ({
        adr: { ...defaultAdr },
      })
      expect(await ev.getLocation()).toEqual([
        'testname',
        'teststreet',
        'testsuburb',
        'teststate',
        'testcountry',
      ].join('\n'))
    })
    test('without name', async () => {
      ev.getFrontmatter = async () => ({
        adr: _.omit(defaultAdr, ['name']),
      })
      expect(await ev.getLocation()).toEqual([
        'teststreet',
        'testsuburb',
        'teststate',
        'testcountry',
      ].join('\n'))
    })
    test('without country', async () => {
      ev.getFrontmatter = async () => ({
        adr: _.omit(defaultAdr, ['country']),
      })
      expect(await ev.getLocation()).toEqual([
        'testname',
        'teststreet',
        'testsuburb',
        'teststate',
        'Australia',
      ].join('\n'))
    })
    test('without state, and country', async () => {
      ev.getFrontmatter = async () => ({
        adr: _.omit(defaultAdr, ['state', 'country']),
      })
      expect(await ev.getLocation()).toEqual([
        'testname',
        'teststreet',
        'testsuburb',
        'Victoria',
        'Australia',
      ].join('\n'))
    })
    test('without state', async () => {
      ev.getFrontmatter = async () => ({
        adr: _.omit(defaultAdr, ['state']),
      })
      expect(await ev.getLocation()).toEqual([
        'testname',
        'teststreet',
        'testsuburb',
        'testcountry',
      ].join('\n'))
    })
  })
})

describe('Calendar', () => {
  const startStr = '2020-02-16T15:00:00'
  const endStr = '2020-02-16T18:00:00'
  describe('addEvent()', () => {
    let cal
    let mockEvent
    let mockVals = {}
    beforeEach(() => {
      cal = new Calendar({
        domain: 'example.com',
        timezone: 'Australia/Melbourne',
      })
      mockVals.organizer = {
        name: 'Test',
        email: 'test@example.com',
      }
      mockVals.location = ['line 1', 'line 2'].join('\n')
      mockVals.frontmatter = {
        title: 'Mosh!!',
        timezone: 'Australia/Melbourne',
        eventGroups: ['vicpah', 'victoria', 'mosh'],
      }
      mockEvent = {
        getSlug: () => 'testslug',
        getOrganizer: async () => mockVals.organizer,
        getLocation: async () => mockVals.location,
        getFrontmatter: async () => mockVals.frontmatter,
        getContentHtml: async () => 'content html',
        getContentText: async () => 'content text',
      }
    })
    for (const [start, end, desc] of [
      [startStr, endStr, 'date strings'],
      [new Date(`${startStr}Z`), new Date(`${endStr}Z`), 'date objects'],
    ]) {
      test(`ical.toString() generates correct data using ${desc}`, async () => {
        Object.assign(mockVals.frontmatter, { start, end })
        await cal.addEvent(mockEvent)
        const calStr = cal.ical.toString()
        expect(calStr).toMatch('UID:testslug@example.com')
        expect(calStr).toMatch('DTSTART;TZID=Australia/Melbourne:20200216T150000')
        expect(calStr).toMatch('DTEND;TZID=Australia/Melbourne:20200216T180000')
        expect(calStr).toMatch('SUMMARY:Mosh!!')
        expect(calStr).toMatch('DESCRIPTION:content text')
        expect(calStr).toMatch('LOCATION:line 1\\nline 2')
        expect(calStr).toMatch('X-ALT-DESC;FMTTYPE=text/html:content html')
        expect(calStr).toMatch('ORGANIZER;CN=\"Test\":mailto:test@example.com')
        expect(calStr).toMatch('CATEGORIES:vicpah,victoria,mosh')
        expect(calStr).toMatch('URL;VALUE=URI:http://example.com/events/testslug/')
      })
    }
  })
})
