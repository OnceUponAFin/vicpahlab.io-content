const fs = require('fs').promises;
const fse = require('fs-extra');
const path = require('path');
const walk = require('walkdir');

const { MarkdownFile } = require('../lib/markdown');


const projPath = path.join(__dirname, '..', '..')
const inPath = path.join(projPath, 'content');
const outPath = path.join(projPath, 'build');

const contentExt = '.md'
const indexName = `index${contentExt}`
const indexNames = [
  indexName,
  `_${indexName}`,
]

class ContentRegistry {
  constructor({ rootPath, thisPath }) {
    this.rootPath = rootPath
    this.thisPath = thisPath

    this.pages = {}
    this.registries = {}

    this.addPage = this.addPage.bind(this)
    this._addRelative = this._addRelative.bind(this)
    this.walk = this.walk.bind(this)
    this.create = this.create.bind(this)
  }

  relPath() {
    return path.relative(this.rootPath, this.thisPath)
  }

  async addPage(page) {
    return this._addRelative((await page.parse()).fullSlug, page)
  }
  async _addRelative(contentPath, page) {
    const sepIdx = contentPath.indexOf(path.sep)
    if (sepIdx === -1) {
      const slug = (await page.parse()).slug
      if (this.pages[slug]) {
        throw new Error(`Duplicate page: ${contentPath}`)
      }
      this.pages[slug] = page
    } else {
      const subName = contentPath.substr(0, sepIdx)
      const nextRel = contentPath.substr(sepIdx + 1)
      if (!this.registries[subName]) {
        this.registries[subName] = new ContentRegistry({
          rootPath: this.rootPath,
          thisPath: path.join(this.thisPath, subName),
        })
      }
      await this.registries[subName]._addRelative(nextRel, page)
    }
  }

  walk() {
    return new Promise(resolve => {
      const emitter = walk.walk(this.thisPath)
      const proms = []
      emitter.on('file', thisPath => {
        proms.push((async () => {
          if (!thisPath.endsWith(contentExt)) {
            return
          }
          await this.addPage(new ContentPage({
            rootPath: this.rootPath,
            thisPath,
          }))
        })())
      })
      emitter.on('end', async () => {
        await Promise.all(proms)
        resolve()
      })
    })
  }

  async create(newRootPath) {
    const pagesData = await Promise.all(
      Object.values(this.pages)
        .map(page => page.parse())
    )
    await fse.outputJson(
      path.join(newRootPath, this.relPath(), 'index.json'),
      {
        items: pagesData
          .map(data => data.listData)
          .filter(data => data),
      }
    )
    for (const page of Object.values(this.pages)) {
      await page.create(newRootPath)
    }
    for (const reg of Object.values(this.registries)) {
      await reg.create(newRootPath)
    }
  }
}

class ContentPage {
  constructor({ rootPath, thisPath }) {
    this.rootPath = rootPath
    this.thisPath = thisPath
    this.mdFile = new MarkdownFile(thisPath)

    this.parse = this.parse.bind(this)
    this._mkdir = this._mkdir.bind(this)
    this._cp = this._cp.bind(this)
    this.create = this.create.bind(this)
    this.getFrontmatter = this.mdFile.getFrontmatter
    this.getContentText = this.mdFile.getContentText
    this.getContentHtml = this.mdFile.getContentHtml
  }
  async parse() {
    const relPath = path.relative(this.rootPath, this.thisPath)
    const contentFile = path.basename(this.thisPath)
    const indexFn = indexNames.find(fn => relPath.endsWith(`${path.sep}${fn}`))
    if (relPath === 'index.md') {
      return {
        fullSlug: 'index',
        slug: 'index',
        assetsPath: null,
        isRoot: true,
        listData: null,
        contentFile,
      }
    } else if (indexFn) { // slug-here/index.md
      const fullSlug = relPath.substr(0, relPath.length - (indexFn.length + 1));
      const slug = path.basename(fullSlug)
      return {
        fullSlug,
        slug,
        assetsPath: path.dirname(relPath),
        isRoot: false,
        listData: { slug },
        contentFile,
      }
    } else if (relPath.endsWith(contentExt)) {  // slug-here.md
      const fullSlug = relPath.substr(0, relPath.length - contentExt.length)
      const slug = path.basename(fullSlug)
      return {
        fullSlug,
        slug,
        assetsPath: null,
        isRoot: false,
        listData: { slug },
        contentFile,
      }
    } else {
      throw new Error('Unknown path')
    }
  }
  async _mkdir(newRootPath) {
    return fse.ensureDir(path.join(newRootPath, (await this.parse()).fullSlug))
  }
  async _cp(newRootPath) {
    const { assetsPath, contentFile, fullSlug, isRoot } = await this.parse()
    if (assetsPath) {
      const dir = await fs.readdir(path.join(this.rootPath, assetsPath))
      for (const fn of dir) {
        const fromPath = path.join(this.rootPath, assetsPath, fn)
        let toPath = path.join(newRootPath, assetsPath, fn)

        if (fn === contentFile) {
          // Rename _index.md
          if (fn !== indexName) {
            toPath = path.join(newRootPath, assetsPath, indexName)
          }
        } else if (fn.endsWith(contentExt)) {
          // Ignore non-index content files
          continue
        }
        await fse.copy(fromPath, toPath)
      }
    } else if (isRoot) {
      return fse.copy(this.thisPath, path.join(newRootPath, indexName))
    } else {
      return fse.copy(
        this.thisPath,
        path.join(
          newRootPath,
          fullSlug,
          indexName,
        ),
      )
    }
  }

  async create(newRootPath) {
    await this._mkdir(newRootPath)
    await this._cp(newRootPath)
  }
}

async function main() {
  await fse.emptyDir(outPath)
  const reg = new ContentRegistry({
    rootPath: inPath,
    thisPath: inPath,
  })
  await reg.walk()
  await reg.create(outPath)
}

module.exports = {
  ContentPage, ContentRegistry,
  main,
}

