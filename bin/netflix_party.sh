#!/bin/sh
THISDIR="$(cd "$(dirname "$0")"; pwd)"
cd "$THISDIR/lib"
yarn install
cd "$THISDIR/netflix_party"
yarn install
yarn start "$@"
