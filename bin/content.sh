#!/bin/sh
THISDIR="$(cd "$(dirname "$0")"; pwd)"
cd "$THISDIR/lib"
yarn install
cd "$THISDIR/content"
yarn install
yarn start
