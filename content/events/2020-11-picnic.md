---
title: November Picnic Munch
timezone: australia/melbourne
start: 2020-11-28T13:00:00
end: 2020-11-28T17:00:00
eventGroups: [vicpah, munch]
cost: Free for BYO food, $5 to for the shared food
addr:
  name: Pillars of Wisdom
  street_address: Pillars of Wisdom
  suburb: Melbourne
  state: VIC
tryBookingEid: 686597
---
Awoo! It's our first in person event after lockdown!!!<br/>
VicPAH is having an outdoor picnic munch on the 28th of November.

Our meets are open to all diverse gender, and sexual identities.<br/>
Whether you're an experienced Pup or Handler, or simply curious to see if it's something that may interest you, we have a welcoming and safe space for you to enjoy.<br/>
We've chosen a spot close to public transport, loads of parking spots, and close to accessible toilets, so everyone will be able to make it!

The meeting point will be at the Pillars of Wisdom on the Tan Track, which is on the edge of the Yarra near Alexandra Gardens.<br/>
We may wander a little bit, so jump on the Discord or Telegram group on the day so we can all find each other!

Tickets are required so that we can make sure we don't go over covid safe limits, and it doubles as a list for contact tracing.<br/>
You can either choose to grab a free ticket, or a $5 BBQ ticket. There is a limit on the number of people we can have, so it's first in best dressed.<br/>
It's BYO food and drink if you choose a free ticket.

This event will be following all covid safe guidelines.<br/>
Temperature testing and social distancing, will be enforced. We'll also provide hand sanitiser, and encourage the use of face masks where necessary.<br/>

If you have any questions, concerns, or if you're feeling nervous, please don't hesitate to reach out to [bark@vicpah.org.au](mailto:bark@vicpah.org.au) or via one of our social media groups.
