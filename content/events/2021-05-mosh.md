---
title: May Mosh
timezone: australia/melbourne
start: 2021-05-16T13:00:00
end: 2021-05-16T17:00:00
eventGroups: [vicpah, mosh]
cost: $15 for members, $20.30 for non-members (pizza $5.30 for members, $7.10 for non-members)
dressCode: |-
  This is our private mosh venue, so leather, rubber, neoprene gear is acceptable! No nudity.
adr:
  name: The Club
  street_address: 7/2D Indwe St
  suburb: West Footscray
  state: VIC
tryBookingEid: 743912
---
VicPAH is having an indoor mosh/munch on the 16th of May.

Our meets are open to all diverse gender, and sexual identities.<br/>
Whether you're an experienced Pup or Handler, or simply curious to see if it's something that may interest you, we have a welcoming and safe space for you to enjoy.<br/>

Tickets are required so that we can make sure we don't go over covid safe limits, and it doubles as a list for contact tracing.

This event will be following all covid safe guidelines.<br/>
Temperature testing and social distancing, will be enforced. We'll also provide hand sanitiser, and encourage the use of face masks where necessary.<br/>

If you have any questions, concerns, or if you're feeling nervous, please don't hesitate to reach out to [bark@vicpah.org.au](mailto:bark@vicpah.org.au) or via one of our social media groups.
