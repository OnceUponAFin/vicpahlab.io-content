---
title: APHC victory mosh and BBQ
timezone: australia/adelaide
start: 2019-11-17T14:00:00
end: 2019-11-17T20:00:00
eventGroups: [aphc, interstate, major]
link: https://www.geardadelaide.com/events/geard-recovery-bbq
sendTelegram: true
adr:
  name: The Wakefield Hotel
  street_address: 76 Wakefield Street
  suburb: Adelaide
  state: South Australia
---
Time to recover and play with all the puppies before heading back to our muggle
lives, Beers and Cocktails will flow, plus a free BBQ.
