---
title: Midsumma pride march
timezone: australia/melbourne
start: '2022-03-13T09:30:00'
end: '2022-03-13T11:00:00'
link: https://chilloutfestival.com.au/product/chillout-street-parade
facebook: https://www.facebook.com/chilloutfestival
eventGroups: [vicpah, victoria, chillout, major, free]
dressCode: |-
  Please wear either puppy gear or something that shows off your puppy pride.
adr:
  street_address: Vincent Street
  suburb: Daylesford
  state: VIC
---
Awoo! VicPAH Will be marching in the Chillout Pride March 2022!
Daylesford on the 13th of March.
Lineup assembly is from 9:30am-10am, and we march from 10:30am-11am.

Join VicPAH and the kennel and show your pride in regional Vic!
