---
title: Gear'd VIP party
timezone: australia/adelaide
start: 2019-11-14T18:00:00
end: 2019-11-14T19:30:00
eventGroups: [aphc, interstate, major]
link: https://www.geardadelaide.com/events/invite-only-vip-party
---
Exclusive invite only VIP party at a surprise secret location for VIP ticket
holders and special guests.
