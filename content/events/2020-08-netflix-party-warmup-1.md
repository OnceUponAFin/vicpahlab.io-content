---
title: Netflix Party - Warm Up (Rick and Morty)
timezone: australia/melbourne
start: 2020-08-01T16:30:00
end: 2020-08-01T16:52:00
eventGroups: [vicpah, netflix]
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

To start off to make sure everyone is able to join and get setup before the
movies start, we'll be running an episode of Rick and Morty.

## Party link
[Click here to join](https://www.netflix.com/watch/80098733?npSessionId=949dc45955741b95&npServerId=s38)

Remember to click the Netflix Party extension icon to join the party!
