---
title: Netflix Party - Monty Python's Life of Brian
timezone: australia/melbourne
start: 2020-08-01T19:15:00
end: 2020-08-01T20:48:00
eventGroups: [vicpah, netflix]
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

Our second movie for the day will be Monty Python's Life of Brian.

## Party link
[Click here to join](https://www.netflix.com/watch/699257?npSessionId=b440f00acd594ec1&npServerId=s88)

Remember to click the Netflix Party extension icon to join the party!

## Synopsis
Born in a stable in Judea, Brian grows up to join a group of anti-Roman zealots, but his fate keeps getting confused with that of a certain carpenter.
