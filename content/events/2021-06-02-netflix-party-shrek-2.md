---
title: "Netflix Party - Shrek 2"
timezone: australia/melbourne
start: 2021-06-02T19:00:00
end: 2021-06-02T20:33:00
eventGroups: [vicpah, netflix]
movieId: 89680
synopsisText: "Shrek, Fiona and Donkey set off to Far, Far Away to meet Fiona's mother and father. But not everyone is happy. Shrek and the King find it hard to get along, and there's tension in the marriage. The fairy godmother discovers that Shrek has married Fiona instead of her Son Prince Charming and sets about destroying their marriage."
partyLink: https://www.tele.pe/netflix/2692fd1f3265b405?s=s165
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

We will only be watching 1 movie for tonight: Shrek 2

## Party link
[Click here to join](https://www.tele.pe/netflix/2692fd1f3265b405?s=s165)

Remember to click the Netflix Party extension icon to join the party!

## Synopsis
Shrek, Fiona and Donkey set off to Far, Far Away to meet Fiona's mother and father. But not everyone is happy. Shrek and the King find it hard to get along, and there's tension in the marriage. The fairy godmother discovers that Shrek has married Fiona instead of her Son Prince Charming and sets about destroying their marriage.
