---
title: "Netflix Party - The Mitchells vs. the Machines"
timezone: australia/melbourne
start: 2021-07-20T19:00:00
end: 2021-07-20T20:54:00
eventGroups: [vicpah, netflix]
movieId: 370144
synopsisText: "A quirky, dysfunctional family's road trip is upended when they find themselves in the middle of the robot apocalypse and suddenly become humanity's unlikeliest last hope."
partyLink: https://www.tele.pe/join/3b2ba187b68b5bea
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

We will only be watching 1 movie for tonight: The Mitchells vs. the Machines

## Party link
[Click here to join](https://www.tele.pe/join/3b2ba187b68b5bea)

Remember to click the Netflix Party extension icon to join the party!

## Synopsis
A quirky, dysfunctional family's road trip is upended when they find themselves in the middle of the robot apocalypse and suddenly become humanity's unlikeliest last hope.
