---
title: APHC pup walk
timezone: australia/adelaide
start: 2019-11-15T15:00:00
end: 2019-11-15T16:00:00
eventGroups: [aphc, interstate, major]
link: https://www.geardadelaide.com/events/aphc-pup-walk
adr:
  name: The Wakefield Hotel
  street_address: 76 Wakefield Street
  suburb: Adelaide
  state: South Australia
---
Meet us at the Wakefield Hotel and join the puppies on the Australian Pup and
Handler Pup Walk.
