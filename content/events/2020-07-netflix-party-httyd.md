---
title: Netflix Party - How to Train Your Dragon
timezone: australia/melbourne
start: 2020-07-18T16:30:00
end: 2020-07-18T18:09:00
eventGroups: [vicpah, netflix]
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

Our first movie for the day will be How to Train Your Dragon.

## Party link
[Click here to join](https://www.netflix.com/watch/70109893?npSessionId=b797d2dbf351d595&npServerId=s96)

Remember to click the Netflix Party extension icon to join the party!

## Synopsis
A hapless young Viking who aspires to hunt dragons becomes the unlikely friend of a young dragon himself, and learns there may be more to the creatures than he assumed. 
