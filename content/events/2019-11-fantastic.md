---
title: Fantastic
timezone: australia/melbourne
start: 2019-11-30T17:00:00
end: 2019-12-01T02:00:00
eventGroups: [victoria, major]
link: https://www.fantasticparty.com.au/
adr:
  name: Club 80
  street_address: 10 Peel St
  suburb: Collingwood
  state: Victoria
---
Eagle Leather and Club 80 reunite to bring you an evening where fetish, dance and colour collide. Enter a fantastic world of the visually extraordinary. Feel the music pulse as you roam, discovering performance installations to elate the senses. For the first time in FANTASTIC's history, we'll take over all three levels of Club 80; so you can roam the upper floors, explore its cavernous tunnels, watch, cruise and play. Fantasy is reality. Lick the sky, eat the sun. This is the FANTASTIC.
