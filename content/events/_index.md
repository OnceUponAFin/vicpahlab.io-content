---
outputs:
- HTML
- Calendar
- JSON
---
VicPAH runs many of it's own events, and collects some others that might be of
interest to our members.

All the coloured tags underneath the events are clickable. If you only want
to see VicPAH events, click a vicpah tag!
