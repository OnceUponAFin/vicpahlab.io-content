---
title: "Netflix Party - Over the Hedge"
timezone: australia/melbourne
start: 2020-11-21T17:00:00
end: 2020-11-21T18:20:00
eventGroups: [vicpah, netflix]
movieId: 31704
synopsisText: "A scheming raccoon fools a mismatched family of forest creatures into helping him repay a debt of food, by invading the new suburban sprawl that popped up while they were hibernating – and learns a lesson about family himself."
partyLink: https://www.tele.pe/netflix/7a816c6afb3f4c96?s=s164
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

Our first movie for the day will be Over the Hedge

## Party link
[Click here to join](https://www.tele.pe/netflix/7a816c6afb3f4c96?s=s164)

Remember to click the Netflix Party extension icon to join the party!

## Synopsis
A scheming raccoon fools a mismatched family of forest creatures into helping him repay a debt of food, by invading the new suburban sprawl that popped up while they were hibernating – and learns a lesson about family himself.
