---
title: APHC Pup, and Handler 2019!
date: 2019-11-22
sendTelegram: true
---
APHC is over, and we have 2 new Australian title holders. Our very
own VicPAH handler: Mickey D, and WAPAH's pup: Dodger!

<!--more-->

<div style="text-align: center">
<img
  src="./IMG_1236_sm.JPG"
  title="APHC 2019 Pup, and Handler"
  alt="APHC 2019 Pup, and Handler"
/>
</div>

It was a close competition all around, and every one of the competitors
did an amazing job. All round, the whole competiotion was a celebration
of the inclusiveness, diversity, and uniqueness that defines our
whole community.
