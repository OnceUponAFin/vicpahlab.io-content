---
title: VicPAH an incorporated association
date: 2020-01-14
---
Big Barking Announcement!

Woof and Bark Victorian Pups and Handlers!
Amazing to see so many of you at the mosh today, had an amazing time wruff and tumbling with you! 

For those of you who weren’t at the mosh today and missed Pup Schisms announcement it is my very great pleasure to be able to announce that as of the 2nd January we are officially "Victorian Pups and Handlers Incorporated"!

<!--more-->

We have opened up registrations for members. I will create a seperate post with details regarding that. 
This is a very very exciting time for VIC-PAH please watch this space more details to come!

See you all at Midsummer!
