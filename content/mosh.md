---
title: Monthly Mosh
layout: eventinfo
eventGroup: mosh
---
Put your best Paw forward and let your inner Puppy loose for a few hours amongst
friends and like-minded peeps, pups, and handlers.

Come and experience VicPAH's monthly puppy mosh.

Our moshes are open to all diverse gender, and sexual identities whether you're
an experienced pup or handler, or simply curious to see if it's something that
may interest you, we have a welcoming and safe space for you to enjoy.

Expect to see human pups and handlers exploring what gets them into their
headspace, some getting pats and belly rubs, while others wrestle and explore
their wruff side.

We have squeeky, squishy, and fluffy puppy toys for your playful pleasure, but
feel free to bring your own special toys and accessories.

## Dress code
Anything from comfortable clothes, to your finest leather or rubber, and
anything in-between.

We don't allow buckles, shoes, jockstraps, or hard/sharp areas in the mosh area.

Otherwise, as long as you aren't exposing your privates, there are no rules on
what anyone can or can't wear.

## Cost
Entry is $15.

This is a VicPAH community event. All non-expenditure proceeds go to the VicPAH
kitty.

## Basic VicPAH and house rules/etiquitte
- No buckles or hard shoes in the moshe
- No exposed genitalia in the mosh pit
- Footwear must be worn at all times, except in the mosh pit
- Respect the people and environment you're in at all times
- Respect yourself at all times
- No illicit substances or sexual activity on premises
- No cameras or recording devices permitted without organisers' consent
- On occasion there will be an in-hous photographer in attendance. Be assured
  that you will not be in view without your knowledge or unless you request it
- Play safe
- Have fun
