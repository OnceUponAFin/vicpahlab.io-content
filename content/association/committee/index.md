---
title: Committee members
layout: committee
members:
- name: Schism
  role: President
  image: schism.jpg
  prefix: He/Him
  email: president@vicpah.org.au
- name: Wolfio
  role: Vice-President
  prefix: He/Him
  email: vicepresident@vicpah.org.au
- name: Chalk
  role: Secretary
  image: chalk.jpg
  prefix: He/Him
  email: secretary@vicpah.org.au
- name: Tropes
  role: Treasurer
  image: tropes.jpg
  prefix: He/Him
  email: treasurer@vicpah.org.au
- name: Morty
  role: General member
  image: morty.jpg
  prefix: He/Him
- name: Neliak
  role: General member
  image: neliak.jpg
  prefix: He/Him
- name: Biscuit
  role: General member
  image: biscuit.jpg
  prefix: He/Him
- name: Mickey D
  role: General member 
  prefix: Zi/Zir
---
# VicPAH committee members
Committee members meet at least once monthly to discuss relevant business relating to the
running of VicPAH, as well as ensuring that all VicPAH-run events run smoothly.
