---
title: Netflix Party Placeholder
timezone: australia/melbourne
start: 2020-12-19T17:00:00
end: 2020-12-19T23:00:00
eventGroups: [vicpah, netflix]
pollId: dG9TrA2tG69GW4d2wEgp
pollLabel: Vote for movies
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

We'll update our events when we've all had a chance to vote on the movies we'll
be watching!
